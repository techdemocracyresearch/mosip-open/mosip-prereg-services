swagger: '2.0'
info:
  description: Api Documentation
  version: '1.0'
  title: Api Documentation
  termsOfService: 'urn:tos'
  contact: {}
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0'
host: 'integ.mosip.io:443'
basePath: /datasync
tags:
  - name: Data-Sync
    description: Data Sync Controller
paths:
  /v0.1/pre-registration/data-sync/datasync:
    get:
      tags:
        - Data-Sync
      summary: Retrieve Pre-Registrations
      operationId: retrievePreRegistrationsUsingGET
      produces:
        - application/json
      parameters:
        - name: pre_registration_id
          in: query
          description: pre_registration_id
          required: true
          type: string
      responses:
        '200':
          description: Data Sync records fetched
          schema:
            $ref: '#/definitions/MainResponseDTO«PreRegArchiveDTO»'
        '400':
          description: Unable to fetch the records
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /v0.1/pre-registration/data-sync/retrieveAllPreRegIds:
    post:
      tags:
        - Data-Sync
      summary: Fetch all PreRegistrationIds
      operationId: retrieveAllPreRegidsUsingPOST
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: dataSyncDto
          description: dataSyncDto
          required: true
          schema:
            $ref: '#/definitions/MainRequestDTO«DataSyncRequestDTO»'
      responses:
        '200':
          description: All PreRegistrationIds fetched successfully
          schema:
            $ref: '#/definitions/MainResponseDTO«PreRegistrationIdsDTO»'
        '201':
          description: Created
        '400':
          description: 'Unable to fetch PreRegistrationIds '
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /v0.1/pre-registration/data-sync/reverseDataSync:
    post:
      tags:
        - Data-Sync
      summary: Store consumed Pre-Registrations
      operationId: storeConsumedPreRegistrationsIdsUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - in: body
          name: consumedData
          description: consumedData
          required: true
          schema:
            $ref: '#/definitions/MainRequestDTO«ReverseDataSyncRequestDTO»'
      responses:
        '200':
          description: Consumed Pre-Registrations saved
          schema:
            $ref: '#/definitions/MainResponseDTO«ReverseDatasyncReponseDTO»'
        '201':
          description: Created
        '400':
          description: Unable to save the records
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
definitions:
  DataSyncRequestDTO:
    type: object
    properties:
      registration-client-id:
        type: string
        description: Registration client id
      from-date:
        type: string
        description: From date
      to-date:
        type: string
        description: To date
      user-id:
        type: string
        description: Registration client user Id
    title: DataSyncRequestDTO
  ExceptionJSONInfoDTO:
    type: object
    properties:
      errorCode:
        type: string
      message:
        type: string
    title: ExceptionJSONInfoDTO
  MainRequestDTO«DataSyncRequestDTO»:
    type: object
    properties:
      id:
        type: string
        description: request id
      ver:
        type: string
        description: request ver
      reqTime:
        type: string
        format: date-time
        description: request time
      request:
        description: request
        $ref: '#/definitions/DataSyncRequestDTO'
    title: MainRequestDTO«DataSyncRequestDTO»
  MainRequestDTO«ReverseDataSyncRequestDTO»:
    type: object
    properties:
      id:
        type: string
        description: request id
      ver:
        type: string
        description: request ver
      reqTime:
        type: string
        format: date-time
        description: request time
      request:
        description: request
        $ref: '#/definitions/ReverseDataSyncRequestDTO'
    title: MainRequestDTO«ReverseDataSyncRequestDTO»
  MainResponseDTO«PreRegArchiveDTO»:
    type: object
    properties:
      err:
        description: Error Details
        $ref: '#/definitions/ExceptionJSONInfoDTO'
      status:
        type: boolean
        description: Response Status
      resTime:
        type: string
        description: Response Time
      response:
        description: Response
        $ref: '#/definitions/PreRegArchiveDTO'
    title: MainResponseDTO«PreRegArchiveDTO»
  MainResponseDTO«PreRegistrationIdsDTO»:
    type: object
    properties:
      err:
        description: Error Details
        $ref: '#/definitions/ExceptionJSONInfoDTO'
      status:
        type: boolean
        description: Response Status
      resTime:
        type: string
        description: Response Time
      response:
        description: Response
        $ref: '#/definitions/PreRegistrationIdsDTO'
    title: MainResponseDTO«PreRegistrationIdsDTO»
  MainResponseDTO«ReverseDatasyncReponseDTO»:
    type: object
    properties:
      err:
        description: Error Details
        $ref: '#/definitions/ExceptionJSONInfoDTO'
      status:
        type: boolean
        description: Response Status
      resTime:
        type: string
        description: Response Time
      response:
        description: Response
        $ref: '#/definitions/ReverseDatasyncReponseDTO'
    title: MainResponseDTO«ReverseDatasyncReponseDTO»
  PreRegArchiveDTO:
    type: object
    properties:
      pre-registration-id:
        type: string
        description: Pre-Registration-ID
      registration-client-id:
        type: string
        description: Registration-Client-Id
      appointment-date:
        type: string
        description: Appointment-Date
      from-time-slot:
        type: string
        description: from-time-slot
      to-time-slot:
        type: string
        description: to-time-slot
      zip-filename:
        type: string
        description: zip-filename
      zip-bytes:
        type: string
        format: byte
        description: zip-bytes
    title: PreRegArchiveDTO
  PreRegistrationIdsDTO:
    type: object
    properties:
      transactionId:
        type: string
        description: Transaction ID
      countOfPreRegIds:
        type: string
        description: Count Of PreRegIds
      preRegistrationIds:
        type: object
        description: Pre-Registration Ids
        additionalProperties:
          type: string
    title: PreRegistrationIdsDTO
  ReverseDataSyncRequestDTO:
    type: object
    properties:
      createdBy:
        type: string
      createdDateTime:
        type: string
        format: date-time
      langCode:
        type: string
      preRegistrationIds:
        type: array
        items:
          type: string
      updateBy:
        type: string
      updateDateTime:
        type: string
        format: date-time
    title: ReverseDataSyncRequestDTO
  ReverseDatasyncReponseDTO:
    type: object
    properties:
      alreadyStoredPreRegIds:
        type: string
      countOfStoredPreRegIds:
        type: string
      transactionId:
        type: string
    title: ReverseDatasyncReponseDTO