package io.mosip.preregistration.generateQRcode.dto;

import lombok.Data;

/**
 * @author Sanober Noor
 *
 */

@Data
public class QRCodeResponseDTO {
/**
 * 
 */
byte[] qrcode;
}
