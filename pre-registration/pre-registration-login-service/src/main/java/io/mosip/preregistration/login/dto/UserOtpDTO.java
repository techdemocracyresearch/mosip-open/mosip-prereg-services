package io.mosip.preregistration.login.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * This DTO class is used to define the initial request parameters.
 * 
 * @author Akshay Jain
 * @since 1.0.0
 *
 */

@Getter
@Setter
public class UserOtpDTO {
	private UserOtp request;
}
