package io.mosip.preregistration.notification.dto;

import lombok.Data;

/**
 * @author Sanober Noor
 *
 */

@Data
public class QRCodeResponseDTO {
/**
 * 
 */
byte[] qrcode;
}
