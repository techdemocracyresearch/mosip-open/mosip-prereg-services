# MOSIP Pre Registration Services

## Introduction

MOSIP is Modular Open Source Identity Platform, read and learn more about it in the extensive [documentation](https://github.com/mosip-open/Documentation). The ***seed contribution to MOSIP is still work in progress***, consider this repo as an early preview for now. Today, we aim to give the community a sense of early direction with this preview. 

The issue list is open, but we will not act upon the issues till a formal release is behind us.

Pre-Registration is a web services system that supports the Pre-Registration web site, the system designed to reach out to population at large and help ID seekers find and book a convenient appointment at a nearby registration center.

## Pre-Registration Services Module

Pre-registration is the module which is the web channel of the MOSIP. This module enables an ID seeker (the end user of MOSIP) to do the following:

1. Book an appointment for one or many Individuals for registration.
1. Enter their demographic details and book appointment by choosing a suitable registration center and time slot
1. Notifies user on a successful booking
1. This module also has the provision for appointment rescheduling and cancellation
1. It has the capability to sync data with registration client

Start with the Pre Registration services [wiki page](https://github.com/mosip-open/Documentation/wiki/FRS-Pre-Registration) and follow up with [design](https://github.com/mosip-open/mosip-prereg-services/blob/master/docs/design/pre-registration/pre-registration-architecture.md)

